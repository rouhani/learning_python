# Demonstrate the usage of namdtuple objects

import collections


# create a Point namedtuple
Point = collections.namedtuple("Point", "x y z")

p1 = Point(10, 20, 19)
p2 = Point(30, 40, 20)

print(p1, p2)
print(p1.x, p1.y, p1.z)

# use _replace to create a new instance
p1 = p1._replace(x=100)
print(p1)
