#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 15:46:31 2019

@author: berkunis
"""
##############################################01_02_PythonLibraries#####################################################
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
import matplotlib.pyplot as plt 

# import data
data = pd.read_csv(r"./predictive_analytics/Datasets/insurance_upd.csv")

# see the first 15 lines of data
print(data.head(15))

# how many values are missing for each column?
count_nan = data.isnull().sum()
print("Before filling",count_nan)

# fill in with mean-value for column 'bmi'
data['bmi'].fillna(data['bmi'].mean(), inplace = True)

# automaticly fill in with mean values for all numeric columns
missing_items = count_nan[count_nan>0].items()
data_types = data.dtypes
for missing_item in missing_items:
    column_name = missing_item[0]
    column_type = data_types[column_name]

    if column_type != "object":
        data[column_name].fillna(data[column_name].mean(), inplace = True)

count_nan = data.isnull().sum()
print("After filling",count_nan)

print(data.head(15))

